class AddBasisToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :basis, :date
  end
end
