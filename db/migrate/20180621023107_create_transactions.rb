class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.references :account, foreign_key: true
      t.references :category, foreign_key: true
      t.date :date
      t.string :title
      t.decimal :value
      t.boolean :done
      t.integer :kind

      t.timestamps
    end
  end
end
