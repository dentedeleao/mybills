# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

p 'Criando usuário'
admin = User.find_or_create_by(email: 'duarte.angelo@gmail.com')
if admin.id.nil?
  admin.password= '123456'
  admin.password_confirmation= '123456'
  admin.save!
end
p 'FIM - Criando usuário'

p 'Criando Contas'
account1 = Account.find_or_create_by(name: "cc Banco do Brasil")
if account1.id.nil?
  account1.current!
  account1.save!
end

account2 = Account.find_or_create_by(name: "Carteira")
if account2.id.nil?
  account2.current!
  account2.save!
end

account3 = Account.find_or_create_by(name: "Órama")
if account3.id.nil?
  account3.investment!
  account3.save!
end
p 'FIM - Criando Contas'

p 'Criando Categorias'
categories = ['Alimentação',
              'Combustível',
              'Telefone']
categories.each do |category|
  Category.find_or_create_by(name: category)
end
p 'FIM - Criando Categorias'
