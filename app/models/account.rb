class Account < ApplicationRecord

  validates :name, presence: true, length: {minimum: 5}
  validates :kind, presence: true

  enum kind: {current: 0, investment: 1}

  has_many :transactions

  # TODO: COLOCAR O NOME DO MÉTODO NO PADRÃO EM INGLÊS
  def calculates_balance
    self.transactions.sum(:value)
  end
end
