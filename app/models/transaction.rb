class Transaction < ApplicationRecord
  validates :account, presence: true
  validates :category, presence: true
  validates :date, presence: true
  validates :title, presence: true, length: {minimum: 3}
  validates :kind, presence: true
  validates :value, presence: true
  validates :basis, presence: true

  validate :validate_value
  belongs_to :account
  belongs_to :category

  enum kind: { debit: 0, credit: 1 }

  def validate_value
    if self.debit? && self.value > 0
      self.value *= -1
    elsif self.credit? && self.value < 0
      self.value *= -1
    end
  end
end
