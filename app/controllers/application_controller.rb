class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  protect_from_forgery with: :exception

  layout :layout_by_resource

  private

  # Layout per resource_name
  def layout_by_resource
    if devise_controller?
      "application_devise"
    else
      "application"
    end
  end

end
