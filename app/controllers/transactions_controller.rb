class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]

  # GET /transactions
  # GET /transactions.json
  def index
    conditions = " 1 = 1 "
    if !params['transaction'].nil?
      if !params['transaction']['initial_date'].nil? && !params['transaction']['initial_date'].empty?
        conditions += " AND date >= '#{Date.parse(params['transaction']['initial_date']).strftime('%Y-%m-%d')}' "
      end
      if !params['transaction']['final_date'].nil? && !params['transaction']['final_date'].empty?
        conditions += " AND date <= '#{Date.parse(params['transaction']['final_date']).strftime('%Y-%m-%d')}' "
      end

      if !params['transaction']['basis_initial_date'].nil? && !params['transaction']['basis_initial_date'].empty?
        conditions += " AND basis >= '#{Date.parse(params['transaction']['basis_initial_date']).strftime('%Y-%m-%d')}' "
      end
      if !params['transaction']['basis_final_date'].nil? && !params['transaction']['basis_final_date'].empty?
        conditions += " AND basis <= '#{Date.parse(params['transaction']['basis_final_date']).strftime('%Y-%m-%d')}' "
      end

    else
      conditions += " AND date >= '#{Date.today.beginning_of_month.strftime('%Y-%m-%d')}' "
      conditions += " AND date <= '#{Date.today.end_of_month.strftime('%Y-%m-%d')}' "
    end

    p conditions

    @transactions = Transaction.where(conditions).order(:date)
    @balance = @transactions.sum(:value)
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to transactions_url, notice: I18n.t('messages.created') }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to transactions_url, notice: I18n.t('messages.updated') }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: I18n.t('messages.destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:account_id, :category_id, :date, :basis, :title, :value, :done, :kind)
    end
end
