json.extract! transaction, :id, :account_id, :category_id, :date, :title, :value, :done, :kind, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
