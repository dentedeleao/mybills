module TransactionsHelper
  OptionsForTransactions = Struct.new(:id, :description)

  def options_for_kinds
    Transaction.kinds_i18n.map { |key, value| OptionsForTransactions.new(key, value) }
  end

end
