module AccountsHelper
  OptionsForKinds = Struct.new(:id, :description)
  OptionsForAccounts = Struct.new(:id, :description)

  def options_for_kinds
    Account.kinds_i18n.map { |key, value| OptionsForKinds.new(key, value) }
  end

  def options_for_accounts
    Account.all.order(:name).map {|acc| OptionsForAccounts.new(acc.id, acc.name) }
  end

end
