module CategoriesHelper
  OptionsForCategory = Struct.new(:id, :description)

  def options_for_categories
    Category.all.order(:name).map { |cat| OptionsForCategory.new(cat.id, cat.name)}
  end
end
